$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover({});
    $('.carousel').carousel({
        interval: 3000
    });

    $('#modalinfo').on('show.bs.modal', function (e){
        console.log('El modal se esta mostrando');

        $('#infobtn').removeClass('btn-info');
        $('#infobtn').addClass('btn-danger');
    });

    $('#modalinfo').on('shown.bs.modal', function (e){
        console.log('El modal se mostro');
    });

    $('#modalinfo').on('hide.bs.modal', function (e){
        console.log('El modal se oculta');
    });
    $('#modalinfo').on('hidden.bs.modal', function (e){
        console.log('El modal se ocultó');
    });
});