const { trim } = require('jquery');

module.exports = function(grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: { 
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: { //browser file
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
            options: {
                watchTask: true,
                server:{
                    baseDir: './' //directorio base para nuestro servidor
                }
            }
          }
        },

        imagemin:{ // comprimir imagenes
            dynamic:{
                files:[{
                    expand: true,
                    cwd: './',
                    src: 'img/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/'
                }]
            }
        },

        copy:{
            html:{
                files: [{
                    expand: true,
                    dot: true,
                    cwd:'./',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [//Para iconos de fuentes 
                    {
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                },
                {
                expand: true,
                dot: true,
                cwd: 'node_modules/@fortawesome/fontawesome-free',
                src: ['webfonts/*.*'],
                dest: 'dist'}
            ]}
        },

        clean: {
            build:{
                src: ['dist/']
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },

            release: {
                //filerev:liberar hashes(md5) a todos los archivos (Img, js y css)
                // en el dist directorio
                files: [{
                    src:[
                        'dist/js/*.js',
                        'dist/css/*.css'
                    ]
                }]
            }
        },

        concat: {
            options:{
                separator:  ';'
            },
            dist:{}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 
                      'noticia.html', 
                      'precio.html', 
                      'register.html' ]
            },
                options:{
                 flow:{
                    steps:{
                      css:['cssmin'],
                       js:['uglify']
                    },
                    post:{
                       css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },

        usemin: {
            html: ['dist/index.html', 'dist/noticia.html', 'dist/precio.html', 'dist/register.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']
            }
        }
    });

    
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ])

};